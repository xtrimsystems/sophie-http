import { DELETE, RequestMethod } from './RequestMethod';

export class DeleteRequestMethod implements RequestMethod
{
	public asString (): string
	{
		return DELETE;
	}
}
