import { GET, RequestMethod } from './RequestMethod';

export class GetRequestMethod implements RequestMethod
{
	public asString (): string
	{
		return GET;
	}
}
