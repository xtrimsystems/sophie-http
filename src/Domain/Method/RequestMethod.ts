export const DELETE = 'DELETE';
export const GET = 'GET';
export const OPTIONS = 'OPTIONS';
export const PATCH = 'PATCH';
export const PUT = 'PUT';
export const POST = 'POST';

export interface RequestMethod
{
	asString (): string;
}
