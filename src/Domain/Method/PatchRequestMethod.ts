import { PATCH, RequestMethod } from './RequestMethod';

export class PatchRequestMethod implements RequestMethod
{
	public asString (): string
	{
		return PATCH;
	}
}
