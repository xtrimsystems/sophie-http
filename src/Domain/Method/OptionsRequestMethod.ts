import { OPTIONS, RequestMethod } from './RequestMethod';

export class OptionsRequestMethod implements RequestMethod
{
	public asString (): string
	{
		return OPTIONS;
	}
}
