import { PUT, RequestMethod } from './RequestMethod';

export class PutRequestMethod implements RequestMethod
{
	public asString (): string
	{
		return PUT;
	}
}
