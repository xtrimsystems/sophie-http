import { POST, RequestMethod } from './RequestMethod';

export class PostRequestMethod implements RequestMethod
{
	public asString (): string
	{
		return POST;
	}
}
