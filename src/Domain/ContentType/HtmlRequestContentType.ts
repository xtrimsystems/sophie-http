import { RequestContentType, TEXT_HTML } from './RequestContentType';

export class HtmlRequestContentType implements RequestContentType
{
	public asString (): string
	{
		return TEXT_HTML;
	}
}
