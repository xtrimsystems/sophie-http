export const APPLICATION_JSON = 'application/json';
export const TEXT_HTML = 'text/html';

export interface RequestContentType
{
	asString (): string;
}
