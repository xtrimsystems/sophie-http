import { APPLICATION_JSON, RequestContentType } from './RequestContentType';

export class JsonRequestContentType implements RequestContentType
{
	public asString (): string
	{
		return APPLICATION_JSON;
	}
}
