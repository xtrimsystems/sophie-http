export class XhrResponse
{
	public headers: string;
	public text: string;
	public type: string;
	public status: number;
	public statusText: string;

	public static fromXhr (xhr: XMLHttpRequest): XhrResponse
	{
		const xhrResponse = new XhrResponse();
		xhrResponse.headers = xhr.getAllResponseHeaders();
		xhrResponse.text = xhr.responseText;
		xhrResponse.type = xhr.responseType;
		xhrResponse.status = xhr.status;
		xhrResponse.statusText = xhr.statusText;

		return xhrResponse;
	}
}
