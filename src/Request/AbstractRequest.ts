import { RequestContentType } from '../Domain/ContentType';
import { RequestMethod } from '../Domain/Method';
import { STATUS_MULTIPLE_CHOICES, STATUS_OK, XhrResponse } from '../Response';

export abstract class AbstractRequest
{
	protected url: string;

	private static getXMLHttpRequest (
		url: string,
		requestMethod: RequestMethod,
		contentType: RequestContentType,
	): XMLHttpRequest
	{
		const xhr: XMLHttpRequest = new XMLHttpRequest();

		xhr.open(requestMethod.asString(), url, true);
		xhr.setRequestHeader('Content-type', contentType.asString());

		return xhr;
	}

	public setUrl (url: string): void
	{
		this.url = url;
	}

	protected async send (
		requestMethod: RequestMethod,
		contentType: RequestContentType,
		data?: object,
	): Promise<XhrResponse>
	{
		return new Promise<XhrResponse>((resolve, reject) => {

			if (!this.isUrl()) { reject(new Error('Url not set')); }

			const xhr: XMLHttpRequest = AbstractRequest.getXMLHttpRequest(this.url, requestMethod, contentType);

			this.setPromiseResolutionXMLHttpRequest(xhr, resolve, reject);

			if (data) {
				xhr.send(JSON.stringify(data));
			} else {
				xhr.send();
			}
		});
	}

	/* tslint:disable-next-line no-any */
	private setPromiseResolutionXMLHttpRequest (xhr: XMLHttpRequest, resolve: any, reject: any): void
	{
		xhr.onload = () => {
			if (xhr.status < STATUS_OK || xhr.status >= STATUS_MULTIPLE_CHOICES)
			{
				reject(XhrResponse.fromXhr(xhr));
			}
			resolve(XhrResponse.fromXhr(xhr));
		};

		xhr.onerror = () => {
			reject(XhrResponse.fromXhr(xhr));
		};
	}

	private isUrl (): boolean
	{
		return !!this.url;
	}
}
