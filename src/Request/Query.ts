import { RequestContentType } from '../Domain/ContentType';
import { GetRequestMethod } from '../Domain/Method';
import { XhrResponse } from '../Response';
import { AbstractRequest } from './AbstractRequest';

export class Query extends AbstractRequest
{
	public async getData (requestContentType: RequestContentType): Promise<XhrResponse>
	{
		return this.send(new GetRequestMethod(), requestContentType);
	}
}
