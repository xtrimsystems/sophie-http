import { RequestContentType } from '../Domain/ContentType';
import { DeleteRequestMethod, PostRequestMethod, PutRequestMethod } from '../Domain/Method';
import { XhrResponse } from '../Response';
import { AbstractRequest } from './AbstractRequest';

export class Command extends AbstractRequest
{
	public async saveData (
		requestContentType: RequestContentType,
		data?: object,
	): Promise<XhrResponse>
	{
		return this.send(new PostRequestMethod(), requestContentType, data);
	}

	public async updateData (
		requestContentType: RequestContentType,
		data?: object,
	): Promise<XhrResponse>
	{
		return this.send(new PutRequestMethod(), requestContentType, data);
	}

	public async deleteData (
		requestContentType: RequestContentType,
		data?: object,
	): Promise<XhrResponse>
	{
		return this.send(new DeleteRequestMethod(), requestContentType, data);
	}
}
