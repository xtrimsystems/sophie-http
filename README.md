# Sophie HTTP

Provides simple components for creating http requests. Ideal for an API.
It uses **Promises**.

**Part of the framework [Sophie](https://bitbucket.org/xtrimsystems/sophie)**

[![npm version](https://badge.fury.io/js/sophie-http.svg)](https://badge.fury.io/js/sophie-http)
[![npm dependencies](https://david-dm.org/xtrimsystems/sophie-http.svg)](https://www.npmjs.com/package/sophie-http?activeTab=dependencies)
[![npm downloads](https://img.shields.io/npm/dm/sophie-http.svg)](https://www.npmjs.com/package/sophie-http)

## INSTALLATION
```shell
yarn add sophie-http
```

## USAGE

### QUERY
To make a **GET** request use the **Query** object

```typescript
// Import objects
import {
    Query,
    HtmlRequestContentType,
    XhrResponse
} from "sophie-http";

// Instantiate Query
const query: Query = new Query();

// Set the API endpoint
query.setUrl('api/posts');

// Make the request, returns a Promise
query.getData(new HtmlRequestContentType())
	.then((res: XhrResponse) => {
		console.log(res);
	})
	.catch((res: XhrResponse) => {
		console.log(res);
	});
```

### COMMAND
To make a **POST, PUT or DELETE** request use the **Command** object

```typescript
// Import objects
import {
    Command,
    JsonRequestContentType,
    XhrResponse
} from "sophie-http";

// Instantiate Command
const command: Command = new Command();

// Set the API endpoint
command.setUrl('api/posts');

command.saveData(new JsonRequestContentType(), { title: "New post", text: "Lorem Ipsum... " })
	.then((res: XhrResponse) => {
		console.log(res);
	})
	.catch((res: XhrResponse) => {
		console.log(res);
	});

command.updateData(new JsonRequestContentType(), { id: 1, title: "Updated title" })
	.then((res: XhrResponse) => {
		console.log(res);
	})
	.catch((res: XhrResponse) => {
		console.log(res);
	});

command.deleteData(new JsonRequestContentType(), { id: 1 })
	.then((res: XhrResponse) => {
		console.log(res);
	})
	.catch((res: XhrResponse) => {
		console.log(res);
	});

```

## Extending Query or Command
For example, to pre-set the url

```typescript
// Import objects
import {
    Query,
    Command,
    JsonRequestContentType,
    XhrResponse
} from "sophie-http";

const URI_API_POSTS = 'api/posts';

export class PostsQuery extends Query
{
	public constructor ()
	{
		super();
		this.url = URI_API_POSTS;
	}
}

export class PostsCommand extends Command
{
	public constructor ()
	{
		super();
		this.url = URI_API_POSTS;
	}
}

const postsQuery: Query = new PostsQuery();
const postsCommand: Command = new PostsCommand();

postsQuery.getData(new JsonRequestContentType())
    .then((res: XhrResponse) => {
        console.log(res);
    })
    .catch((res: XhrResponse) => {
        console.log(res);
    });

postsCommand.saveData(new JsonRequestContentType(), { title: "New post", text: "Lorem Ipsum... " })
	.then((res: XhrResponse) => {
		console.log(res);
	})
	.catch((res: XhrResponse) => {
		console.log(res);
	});

postsCommand.updateData(new JsonRequestContentType(), { id: 1, title: "Updated title" })
	.then((res: XhrResponse) => {
		console.log(res);
	})
	.catch((res: XhrResponse) => {
		console.log(res);
	});

postsCommand.deleteData(new JsonRequestContentType(), { id: 1 })
	.then((res: XhrResponse) => {
		console.log(res);
	})
	.catch((res: XhrResponse) => {
		console.log(res);
	});


```

## Changelog
[Changelog](https://bitbucket.org/xtrimsystems/sophie-http/src/master/CHANGELOG.md)

## Contributing [![contributions welcome](https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat)](https://github.com/dwyl/esta/issues)

## License
This software is licensed under the terms of the [MIT license](https://opensource.org/licenses/MIT). See [LICENSE](https://bitbucket.org/xtrimsystems/sophie-http/src/master/LICENSE) for the full license.
