/* tslint:disable no-any */
import { JsonRequestContentType } from '../../src/Domain/ContentType';
import { GetRequestMethod } from '../../src/Domain/Method';
import { AbstractRequest } from '../../src/Request';
import { XhrResponse } from '../../src/Response';

class Request extends AbstractRequest
{
	public doSend (): Promise<XhrResponse>
	{
		return this.send(new GetRequestMethod(), new JsonRequestContentType(), {});
	}
}

describe('test AbstractRequest', () => {

	const oldXMLHttpRequest = (window as any).XMLHttpRequest;
	let mockXHR: XMLHttpRequest;
	let mockXHROpen: () => void;
	let mockXHRSend: () => void;
	let mockXHRSetRequestHeader: () => void;
	let mockXHRGetAllResponseHeaders: () => void;

	const createMockXHRSuccess = (
		status: number = 200,
		responseJSON: object = {},
		): any => {
			return {
				open: mockXHROpen,
				send: mockXHRSend,
				setRequestHeader: mockXHRSetRequestHeader,
				readyState: 4,
				responseType: 'json',
				status,
				statusText: 'Ok',
				responseText: JSON.stringify(responseJSON),
				onreadystatechange: function () {
					this.onload();
				},
				getAllResponseHeaders: mockXHRGetAllResponseHeaders,
			};
	};

	const createMockXHRError = (
		status: number = 500,
		responseJSON: object = {},
		): any => {
			return {
				open: mockXHROpen,
				send: mockXHRSend,
				setRequestHeader: mockXHRSetRequestHeader,
				readyState: 4,
				responseType: 'json',
				status: status,
				statusText: 'Server Error',
				responseText: JSON.stringify(responseJSON),
				onreadystatechange: function () {
					this.onerror();
				},
				getAllResponseHeaders: mockXHRGetAllResponseHeaders,
			};
	};

	beforeEach(() => {
		mockXHROpen = jest.fn();
		mockXHRSend = jest.fn();
		mockXHRSetRequestHeader = jest.fn();
		mockXHRGetAllResponseHeaders = jest.fn();
	});

	afterEach(() => {
		(window as any).XMLHttpRequest = oldXMLHttpRequest;
	});

	describe('method "setUrl"', () => {
		it('should set the url', () => {
			const request = new Request();

			request.setUrl('/foo');

			expect((request as any).url).toBe('/foo');
		});
	});
	describe('method "send"', () => {
		it('should make a request and resolve the promise returning a XMLResponse if everything goes smooth', () => {

			mockXHR = createMockXHRSuccess();
			(window as any).XMLHttpRequest = jest.fn(() => mockXHR);

			const request = new Request();

			request.setUrl('/foo');

			const requestPromise = request.doSend();

			(mockXHR as any).onreadystatechange();

			requestPromise
				.then((data) => {
					expect(data).toEqual({
						headers: undefined,
						status: 200,
						statusText: 'Ok',
						text: '{}',
						type: 'json',
					});
				});

			expect(mockXHROpen).toHaveBeenCalled();
			expect(mockXHRSend).toHaveBeenCalled();
			expect(mockXHRSetRequestHeader).toHaveBeenCalled();
			expect(mockXHRGetAllResponseHeaders).toHaveBeenCalled();
		});

		it('should make a request and reject the promise with a successful response but with an error state', () => {

			/* tslint:disable-next-line no-magic-numbers */
			mockXHR = createMockXHRSuccess(302);
			(window as any).XMLHttpRequest = jest.fn(() => mockXHR);

			const request = new Request();

			request.setUrl('/foo');

			const requestPromise = request.doSend();

			(mockXHR as any).onreadystatechange();

			requestPromise
				.catch((e) => {
					expect(e).toEqual({
						headers: undefined,
						status: 302,
						statusText: 'Ok',
						text: '{}',
						type: 'json',
					});
				});

			expect(mockXHROpen).toHaveBeenCalled();
			expect(mockXHRSend).toHaveBeenCalled();
			expect(mockXHRSetRequestHeader).toHaveBeenCalled();
			expect(mockXHRGetAllResponseHeaders).toHaveBeenCalled();
		});

		it('should make a request and reject the promise because of an error', () => {

			mockXHR = createMockXHRError();
			(window as any).XMLHttpRequest = jest.fn(() => mockXHR);

			const request = new Request();

			request.setUrl('/foo');

			const requestPromise = request.doSend();

			(mockXHR as any).onreadystatechange();

			requestPromise
				.catch((e) => {
					expect(e).toEqual({
						headers: undefined,
						status: 500,
						statusText: 'Server Error',
						text: '{}',
						type: 'json',
					});
				});

			expect(mockXHROpen).toHaveBeenCalled();
			expect(mockXHRSend).toHaveBeenCalled();
			expect(mockXHRSetRequestHeader).toHaveBeenCalled();
			expect(mockXHRGetAllResponseHeaders).toHaveBeenCalled();
		});
	});
});
