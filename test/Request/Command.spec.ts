/* tslint:disable no-any no-magic-numbers */
import { instance, mock, when } from "ts-mockito";

import { APPLICATION_JSON, JsonRequestContentType } from "../../src/Domain/ContentType";
import { DeleteRequestMethod, PostRequestMethod, PutRequestMethod } from "../../src/Domain/Method";
import { AbstractRequest, Command } from "../../src/Request";

describe('Test Command', () => {

	let command: Command;
	let spySend: jest.Mock;
	let mockJsonRequestContentType: JsonRequestContentType;
	let jsonRequestContentType: JsonRequestContentType;

	beforeEach(() => {
		command = new Command();
		mockJsonRequestContentType = mock(JsonRequestContentType);
		when(mockJsonRequestContentType.asString()).thenReturn(APPLICATION_JSON);
		jsonRequestContentType = instance(mockJsonRequestContentType);

		spySend = jest.fn();
		(AbstractRequest as any).prototype.send = spySend;
	});

	describe('method "deleteData"', () => {

		it('should throw and error if no url is set', () => {

			const promise = command.deleteData(mockJsonRequestContentType);

			expect(spySend).toHaveBeenCalledTimes(1);
			expect(spySend.mock.calls[0][0]).toBeInstanceOf(DeleteRequestMethod);
			expect(spySend.mock.calls[0][1]).toBe(mockJsonRequestContentType);
			expect(spySend.mock.calls[0][2]).toBe(undefined);

			return promise
				.catch((e: Error) => {
					expect(e).toBeInstanceOf(Error);
					expect(e.message).toBe('Url not set');
				});

		});

		it('should return a Promise<XhrResponse> if the url is set', () => {

			command.setUrl('http://test.test/test');

			const promise = command.deleteData(jsonRequestContentType);
			expect(promise).toBeInstanceOf(Promise);
			expect(spySend).toHaveBeenCalledTimes(1);
			expect(spySend.mock.calls[0][0]).toBeInstanceOf(DeleteRequestMethod);
			expect(spySend.mock.calls[0][1]).toBe(jsonRequestContentType);
			expect(spySend.mock.calls[0][2]).toBe(undefined);
		});
	});

	describe('method "saveData"', () => {

		it('should throw and error if no url is set', () => {

			const promise = command.saveData(mockJsonRequestContentType);

			expect(spySend).toHaveBeenCalledTimes(1);
			expect(spySend.mock.calls[0][0]).toBeInstanceOf(PostRequestMethod);
			expect(spySend.mock.calls[0][1]).toBe(mockJsonRequestContentType);
			expect(spySend.mock.calls[0][2]).toBe(undefined);

			return promise
				.catch((e: Error) => {
					expect(e).toBeInstanceOf(Error);
					expect(e.message).toBe('Url not set');
				});

		});

		it('should return a Promise<XhrResponse> if the url is set', () => {

			command.setUrl('http://test.test/test');

			const promise = command.saveData(jsonRequestContentType);
			expect(promise).toBeInstanceOf(Promise);
			expect(spySend).toHaveBeenCalledTimes(1);
			expect(spySend.mock.calls[0][0]).toBeInstanceOf(PostRequestMethod);
			expect(spySend.mock.calls[0][1]).toBe(jsonRequestContentType);
			expect(spySend.mock.calls[0][2]).toBe(undefined);
		});
	});

	describe('method "updateData"', () => {

		it('should throw and error if no url is set', () => {

			const promise = command.updateData(mockJsonRequestContentType);

			expect(spySend).toHaveBeenCalledTimes(1);
			expect(spySend.mock.calls[0][0]).toBeInstanceOf(PutRequestMethod);
			expect(spySend.mock.calls[0][1]).toBe(mockJsonRequestContentType);
			expect(spySend.mock.calls[0][2]).toBe(undefined);

			return promise
				.catch((e: Error) => {
					expect(e).toBeInstanceOf(Error);
					expect(e.message).toBe('Url not set');
				});

		});

		it('should return a Promise<XhrResponse> if the url is set', () => {

			command.setUrl('http://test.test/test');

			const promise = command.updateData(jsonRequestContentType);
			expect(promise).toBeInstanceOf(Promise);
			expect(spySend).toHaveBeenCalledTimes(1);
			expect(spySend.mock.calls[0][0]).toBeInstanceOf(PutRequestMethod);
			expect(spySend.mock.calls[0][1]).toBe(jsonRequestContentType);
			expect(spySend.mock.calls[0][2]).toBe(undefined);
		});
	});
});
