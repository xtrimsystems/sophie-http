import { instance, mock, when } from "ts-mockito";

import { APPLICATION_JSON, JsonRequestContentType } from "../../src/Domain/ContentType";
import { Query } from '../../src/Request';

describe('Test Query', () => {
	describe('method "getData"', () => {
		it('should throw and error if no url is set', () => {

			const query: Query = new Query();
			const mockJsonRequestContentType = mock(JsonRequestContentType);

			return query.getData(mockJsonRequestContentType)
				.catch((e: Error) => {
					expect(e).toBeInstanceOf(Error);
					expect(e.message).toBe('Url not set');
			});

		});

		it('should return a Promise<XhrResponse> if the url is set', () => {

			const query: Query = new Query();
			const mockJsonRequestContentType = mock(JsonRequestContentType);
			when(mockJsonRequestContentType.asString()).thenReturn(APPLICATION_JSON);

			query.setUrl('http://test.test/test');

			expect(query.getData(instance(mockJsonRequestContentType))).toBeInstanceOf(Promise);

		});
	});
});
