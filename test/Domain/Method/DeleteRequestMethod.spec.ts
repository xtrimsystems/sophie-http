import { DELETE, DeleteRequestMethod } from '../../../src/Domain/Method';

describe('Test value object DeleteRequestMethod', () => {

	const deleteRequestMethod: DeleteRequestMethod = new DeleteRequestMethod();

	describe('Method "asString"', () => {
		it('should return expected string', () => {
			expect(deleteRequestMethod.asString()).toBe(DELETE);
		});
	});
});
