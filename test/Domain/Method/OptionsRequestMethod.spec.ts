import { OPTIONS, OptionsRequestMethod } from '../../../src/Domain/Method';

describe('Test value object OptionsRequestMethod', () => {

	const optionsRequestMethod: OptionsRequestMethod = new OptionsRequestMethod();

	describe('Method "asString"', () => {
		it('should return expected string', () => {
			expect(optionsRequestMethod.asString()).toBe(OPTIONS);
		});
	});
});
