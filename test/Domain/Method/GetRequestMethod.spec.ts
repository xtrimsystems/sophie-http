import { GET, GetRequestMethod } from '../../../src/Domain/Method';

describe('Test value object GetRequestMethod', () => {

	const getRequestMethod: GetRequestMethod = new GetRequestMethod();

	describe('Method "asString"', () => {
		it('should return expected string', () => {
			expect(getRequestMethod.asString()).toBe(GET);
		});
	});
});
