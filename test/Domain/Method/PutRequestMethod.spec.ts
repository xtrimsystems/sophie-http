import { PUT, PutRequestMethod } from '../../../src/Domain/Method';

describe('Test value object PutRequestMethod', () => {

	const putRequestMethod: PutRequestMethod = new PutRequestMethod();

	describe('Method "asString"', () => {
		it('should return expected string', () => {
			expect(putRequestMethod.asString()).toBe(PUT);
		});
	});
});
