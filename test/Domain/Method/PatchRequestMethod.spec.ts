import { PATCH, PatchRequestMethod } from '../../../src/Domain/Method';

describe('Test value object PatchRequestMethod', () => {

	const patchRequestMethod: PatchRequestMethod = new PatchRequestMethod();

	describe('Method "asString"', () => {
		it('should return expected string', () => {
			expect(patchRequestMethod.asString()).toBe(PATCH);
		});
	});
});
