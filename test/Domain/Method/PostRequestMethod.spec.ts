import { POST, PostRequestMethod } from '../../../src/Domain/Method';

describe('Test value object PostRequestMethod', () => {

	const postRequestMethod: PostRequestMethod = new PostRequestMethod();

	describe('Method "asString"', () => {
		it('should return expected string', () => {
			expect(postRequestMethod.asString()).toBe(POST);
		});
	});
});
