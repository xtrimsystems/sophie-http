import { HtmlRequestContentType, TEXT_HTML } from '../../../src/Domain/ContentType';

describe('Test value object HtmlRequestContentType', () => {

	const htmlRequestContentType: HtmlRequestContentType = new HtmlRequestContentType();

	describe('Method "asString"', () => {
		it('should return expected string', () => {
			expect(htmlRequestContentType.asString()).toBe(TEXT_HTML);
		});
	});
});
