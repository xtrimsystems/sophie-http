import { APPLICATION_JSON, JsonRequestContentType } from '../../../src/Domain/ContentType';

describe('Test value object JsonRequestContentType', () => {

	const htmlRequestContentType: JsonRequestContentType = new JsonRequestContentType();

	describe('Method "asString"', () => {
		it('should return expected string', () => {
			expect(htmlRequestContentType.asString()).toBe(APPLICATION_JSON);
		});
	});
});
