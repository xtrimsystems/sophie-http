import { XhrResponse } from '../../src/Response';

describe('Test value object XhrResponse', () => {
	describe('static method "fromXhr"', () => {
		it('should create create an instance of XhrResponse from a XMLHttpRequest object', () => {

			const mockXMLHttpRequest: XMLHttpRequest = new XMLHttpRequest();

			expect(XhrResponse.fromXhr(mockXMLHttpRequest)).toBeInstanceOf(XhrResponse);
		});
	});
});
